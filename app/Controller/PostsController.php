<?php
class PostsController extends AppController {
    public $helpers = array ('Html', 'Form');
    public $component = array ('Paginator');
    var $name = 'Posts';
    function search() {
        $searchid = $_POST['searchid'];
        $searchtitle = $_POST['searchtitle'];
        $searchbody = $_POST['searchbody'];
        if ($searchbody == "" && $searchid == "" && $searchtitle == ""){
            $this->Session->setFlash(('Enter Something...'),'default', array ('class'=>'error-message'));
            $this->redirect(array ('action' => 'index'));
        }
        else {
            if ($searchid != "") {
                $field = "Post.id";
                $value = $searchid;
            };
            if ($searchtitle != "") {
                $field = "title";
                $value = $searchtitle;
             };
            if ($searchbody != "") {
                $field = "body";
                $value = $searchbody;
            };
        }
        $results = $this->Post->find('all', array ('fields' => array ('Post.id','Post.title','Post.body','Post.created','Post.modified'),
                                             'posts' => 'Post.id desc',
                                             'conditions' =>array ($field . ' ' . 'LIKE' => '%'.$value.'%')
                                                // 'conditions' =>( array('id'. ' ' . 'LIKE' => '%'.$id.'%'),array('title' . ' ' . 'LIKE' => '%'.$title.'%'),array('body' . ' ' . 'LIKE' => '%'.$body.'%'))
                                             )
                                );
        $this->set ('results', $results);
    }
    public function index () {
        $this->set('posts', $this->Post->find('all'));
        $this->paginate = array ('limit' => 5, 'order' => array ('posts.id' => 'desc'));
        $posts = $this->paginate('Post');
        $this->set('posts', $posts);
    }
    public function view ($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Post.', true));
            $this->redirect(array ('action' => 'index'));
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);
    
    }
    public function addComment($id = null) {
        if($this->request->is('post')) {
            if (!empty($this->data['Comment'])) {
                $this->request->data['Comment']['class'] = 'Post'; 
                $this->request->data['Comment']['foreign_id'] = $id;
                $this->request->data['Comment']['name'] = $this->Auth->user('username');
                $this->Post->Comment->create(); 
                if(!empty($this->request->data)) {
                    if(!empty($this->request->data['Comment']['upload']['name'])) {
                        $file = $this->request->data['Comment']['upload'];
                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                        $arr_ext = array('jpg', 'jpeg', 'gif');
                        if(in_array($ext, $arr_ext)) {
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                            $this->request->data['Comment']['image'] = "/img/". $file['name'];
                        }
                    }
                }
                if ($this->Post->Comment->save($this->data)) {
                    echo $this->Session->setFlash('The Comment has been saved.', 'default', array('class' => 'success'));
                    $this->redirect(array ('action' => 'view', $id));
                }
                echo $this->Session->setFlash('The Comment could not be saved. Please, try again.', 'default', array('class' => 'error-message'));
            }
            $post = $this->Post->read(null, $id);
            $this->set(compact('post'));
        }
    }
   public function add() {
        if ($this->request->is('post')) {
            $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $this->request->data['Post']['username'] = $this->Auth->user('username');
            if(!empty($this->request->data)) {
                if(!empty($this->request->data['Post']['upload']['name'])) {
                    $file = $this->request->data['Post']['upload'];
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $arr_ext = array('jpg', 'jpeg', 'gif');
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                        $this->request->data['Post']['image'] = "/img/". $file['name'];
                    }
                }
            }
            if ($this->Post->save($this->request->data)) {
                echo $this->Session->setFlash('Your post has been saved', 'default', array('class' => 'success'));
                return $this->redirect(array('action' => 'index'));
            }
                        echo $this->Session->setFlash('Unable to add your post.', 'default', array('class' => 'error-message'));
        }
    }
    public function edit ($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        if(!empty($this->request->data)) {
                if(!empty($this->request->data['Post']['upload']['name'])) {
                    $file = $this->request->data['Post']['upload'];
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $arr_ext = array('jpg', 'jpeg', 'gif');
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                        $this->request->data['Post']['image'] = "/img/". $file['name'];
                        
                    }
                }
            }
        if ($this->request->is(array ('post', 'put'))) {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Session->setFlash('Post updated successfully!', 'default', array ('class'=>'success'));
                return $this->redirect(array ('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'), 'default', array ('class'=>'error-message'));
        }
        if (!$this->request->data) {
            $this->request->data = $post;
        }
        }     
    public function commentEdit($id = null, $foreign_id) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        if(!empty($this->request->data)) {
            if(!empty($this->request->data['Comment']['upload']['name'])) {
                $file = $this->request->data['Comment']['upload'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif');
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                    $this->request->data['Comment']['image'] = "/img/". $file['name'];
                }
            }
        }
        $comment = $this->Post->Comment->findById($id);
        if (!$comment) {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Post->Comment->id = $id;
            
            if ($this->Post->Comment->save($this->request->data)) {
                $this->Session->setFlash('Comment updated successfully!', 'default', array ('class'=>'success'));
                return $this->redirect(array ('action' => 'view', $foreign_id));
            }
        }
        if (!$this->request->data) {
            $this->request->data = $comment;
        }
        $this->set('comment', $comment);
    }



    public function delete ($id, $foreign_id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Post->delete ($id)) {
            $this->Session->setFlash(__('The post  has been deleted.'), 'default', array ('class' => 'success'));
        } 
        else {
            $this->Session->setFlash(__('The post  could not be deleted.'),  'default', array ('class' => 'error-message'));
        }
        return $this->redirect(array ('action' => 'index'));
    }  
    public function deleteComment($id, $foreign_id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Post->Comment->delete($id)) {
            echo  $this->Session->setFlash(__('The comment  has been deleted.'), 'default', array ('class' => 'success'));
        } 
        else {
            $this->Session->setFlash(__('The comment could not be deleted.'),'default', array ('class' => 'error-message'));       
        }
        return $this->redirect(array ('action' => 'view', $foreign_id)); 
    }  
    public function isAuthorized($user) {
        if ($this->action === 'add') {
        return true;
        }
        if (in_array ($this->action, array ('edit', 'delete'))) {
                $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }
}