<?php
  class Post extends AppModel{
	   public $validate = array ('title' => array ('rule' => 'notBlank'),
                              'body' => array ('rule' => 'notBlank')
                              );
    
    public function isOwnedBy($post, $user) {
      return $this->field('id', array ('id' => $post, 'user_id' => $user)) !== false;
    }
      var $name = 'Post';  //comment
      var $hasMany = array (
          'Comment' => array (
          'className' => 'Comment',
          'foreignKey' => 'foreign_id',
          'name' => 'username',
          'conditions' => array ('Comment.class' => 'Post'),
          ),
      );
      var $belongsTo = array (
        'User' => array (
        'className' => 'User',
        'foreignKey' => 'user_id',
        'name' => 'username',
        ),
      );
  }


