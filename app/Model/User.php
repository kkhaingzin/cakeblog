<?php
    App::uses('AppModel', 'Model');
    App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

    class User extends AppModel {
        public $validate = array (
                'username' => array (
                    'required' => array ('rule' => 'notBlank',
                    'message' => 'A username is required')),
                'password' => array ('required' => array ('rule' => 'notBlank',
                    'message' => 'A password is required')),
                'confirm_password' => array ('equaltofield' => array (
                    'rule' => array ('equaltofield','password'),
                    'message' => 'Require the same value to password.',
                    'on' => 'create',)),
                'role' => array (
                    'valid' => array (
                    'rule' => array ('inList', array ('admin', 'author')),
                    'message' => 'Please enter a valid role',
                    'allowEmpty' => false))
        );
    function equaltofield ($check,$otherfield) {
        $fname = '';
        foreach ($check as $key => $value) {
            $fname = $key;
            break;
        }
        return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname];
    } 
public function beforeSave ($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new SimplePasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
    }
    return true;
}
    var $name = 'User';// comment
    var $hasMany = array (
                    'Comment' => array (
                    'className' => 'Comment',
                    'foreignKey' => 'foreign_id',
                    'conditions' => array ('Comment.class'=>'User'),
                    ),
                );
    }