<h1>
    <?php echo h($post['Post']['title']); ?>
</h1>
<p>
    <small> Created: <?php echo $post['Post']['created']; ?> </small>
</p>
<?php  
    if(!empty($post['Post']['image'])) {
        echo $this->HTML->image($post['Post']['image'], array ('border' => 1, 'width' => 500, 'height' => 500)); 
    }
?>
<p>
    <?php echo h($post['Post']['body']); ?>
</p>
<?php
	echo $this->Html->link('Back', array ('action' => 'index'));
?>
<!-- coment form -->
<?php
    echo $this->Form->create('Comment', array ('enctype' => 'multipart/form-data',
        'url' => array ('controller' => 'posts', 'action' => 'addComment', $post['Post']['id'])));
    echo $this->Form->input('Comment.upload', array ('type' => 'file','label' => 'Comment image'));
    echo $this->Form->input('Comment.body', array ('label' => '', 'rows' => '2', 'placeholder' => 'Write a comment...'));
 
    echo $this->form->end('Submit');
 	if ( !empty($post['Comment']) ): 
        foreach ($post['Comment'] as $comment):
            echo '<em><strong>'.$comment['name'].'</strong></em>';
?>
    <br>
	<table>
        <tr>
            <td>
            <?php  
                if(!empty($comment['image'])) {
                    echo $this->Html->image($comment['image'], array ('border' => '1', 'width' => '70', 'height' => '50')); }
                    echo $comment['body']; 
                    $name = $this->Session->read('Auth.User.username');
                    	if ($name == $comment['name']) {
                    		echo $this->Form->postLink('Delete',
            					array ('controller' => 'posts', 'action' => 'deleteComment',
            							$comment['id'], $comment['foreign_id']),
            					array ('confirm' => 'Are u sure'));

                    		echo"|". $this->Form->postLink('Edit',
            					array ('controller' => 'posts', 'action' => 'commentEdit',
            							$comment['id'], $comment['foreign_id']));
                }
            ?>
            </td>
        </tr>
    </table>
        <?php endforeach; ?>
    <?php else: ?>
        <p>No comments...</p>
    <?php endif;
?>
