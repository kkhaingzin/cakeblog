<h1>Add Post</h1>
<?php
echo $this->Form->create('Post', array ('enctype' => 'multipart/form-data'));
echo $this->Form->input('title');
echo $this->Form->input('upload', array ('type' => 'file')); 
echo $this->Form->input('body', array ('rows' => '2'));
echo $this->Form->end('Save Post'); 
echo $this->Html->link('Back',array ('action' => 'index')); 
?>

