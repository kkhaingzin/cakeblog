<h1>Edit Comment</h1>
<?php
echo $this->Form->create ('Comment',array ('enctype' => 'multipart/form-data'));
echo $this->Form->input('upload', array ('type' => 'file'));
echo $this->Form->input ('body', array ('rows' => '3'));
echo $this->Form->input ('id', array ('type' => 'hidden'));
echo $this->Form->end ('Save Comment');
echo $this->Html->link('Back',
            					array ('controller' => 'posts', 'action' => 'view',
            							 $comment['Comment']['foreign_id']));
?>